"use strict";
/*-----------------------------------------------------------------------------
 * @package;    gulp-bump-version
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/
Object.defineProperty(exports, "__esModule", { value: true });
// INCLUDES
const assert = require("assert");
const File = require("vinyl");
const bump = require("../../index.js");
// SANITY CHECK - Makes sure our tests are working proerly
describe('SanityCheck', function () {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});
// Feature Check I
describe('gulp-bump-version I', function () {
    describe('in buffer mode', function () {
        it('Should return 0.1.0 as that is what the regex will match. It checks only that it can match.', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ version: -1 });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), '@version:  0.1.0');
                done();
            });
        });
    });
});
// Feature Check II
describe('gulp-bump-version II', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version has its "patch" revision incremented by 1', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'patch' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.1 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check III
describe('gulp-bump-version III', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version has its "minor" revision incremented by 1', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'minor' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.2.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check IV
describe('gulp-bump-version IV', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version has its "major" revision incremented by 1', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'major' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  1.0.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check V
describe('gulp-bump-version V', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version has its "prerelease" revision incremented by 1', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'prerelease' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0-1 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check VI
describe('gulp-bump-version VI', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version is set to 2.1.31-1', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ version: '2.1.31-1' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  2.1.31-1 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check VII
describe('gulp-bump-version VII', function () {
    describe('in buffer mode', function () {
        it('Should modify the supplied "file" so that the contained version is set to 0.1.1. It uses a customized key with out a semi-colon, and since it was removed from the fake file - it should match.', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'patch', key: '@version' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version  0.1.1 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
// Feature Check VIII
describe('gulp-bump-version VIII', function () {
    describe('in buffer mode', function () {
        it('Should not modify the supplied "file" leaving the version set to 0.1.0. It uses a customized key where the semi-colon has been removed, yet since it exists in our fake file - it should not match.', function (done) {
            // Create the fake file
            let fakeFile = new File({
                contents: new Buffer('This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!')
            });
            let bumper = bump({ type: 'patch', key: '@version' });
            bumper.write(fakeFile);
            bumper.once('data', function (file) {
                assert(file.isBuffer());
                assert.equal(file.contents.toString('utf8'), 'This is just a sample string with a version tag contained in it.' +
                    ' Let us say that " @version:  0.1.0 " is what we are matching, and' +
                    ' that the test ensures it happens even embedded within this sample text.' +
                    ' The test ensures our RegExp is SOLID!');
                done();
            });
        });
    });
});
