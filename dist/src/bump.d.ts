export default class bump {
    /**
     * Represents possible operations that could be performed upon a semantic version string
     *
     * @arg { Object }
     */
    operations: {
        patch: number;
        minor: number;
        major: number;
        version: number;
        prerelease: number;
    };
    /**
     * Represents a semantic version string that will replace another found within a file stream
     *
     * @arg { string }
     */
    replacementSemver: any;
    /**
     * Represents one of the possible operations that could be performed upon a semantic version string
     *
     * @arg { string }
     */
    replacementOperation: any;
    /**
     * This is a regular expression inteded to parse a JSDoc-based version tag and ac-
     * companying value from the header comment of any applicable file.
     *
     *      [NOTES]
     *
     *
     *  (?<required> )+         <-- This is the group we want to exist. We're making
     *                              it have to exist for a match to happen, by surrou-
     *                              nding the entire regex with this capture group.
     *
     *                              - The matching '( and )' makes a 'group'.
     *                              - The '?<xxx>' inside the opening parenthesis _na-
     *                                mes_ the group (xxx canbe whatever).
     *                              - The '+' at the end means the group must exist 1
     *                                or more times.
     *
     *                              * A '?:' inside the opening parenthesis would sig-
     *                                nify NOT to capture anything, typically a nested
     *                                set of parenthesis would then be provided to si-
     *                                gnify _what_ within the non-capturing group, SH-
     *                                OULD be captured, as you'lls ee in a moment.
     *
     *
     *  (?<version> )           <-- Names the version string group
     *
     *
     *  (@version:[ ]*)         <-- Matches literally "@version:" and possibly 0 or
     *                              more spaces
     *
     *                              * We do not use \s, as that includes newlines -
     *                                which we do no want to allow. Therefore, we
     *                                use [ ].
     *                              * The capture (or parenthesis) around this block
     *                                provide us with a group which will contain the
     *                                "@version" and all the spaces that were found
     *                                prior to the major component of our semver stri-
     *                                ng, so that we can build a new, modified, semver
     *                                - without breaking formatting!
     *
     *
     *  (?:(?<major>\d+).){1}   <-- This captures from 1 or more numbers till a period
     *                              (but does not capture the period)
     *
     *                              * The group is named major, the major version com-
     *                                ponent)
     *
     *  (?:(?<minor>\d+).){1}   <-- This captures from 1 or more numbers till a period
     *                              again, again 1 time, yet not the period - again.
     *
     *                              * The group is named minor, the minor version com-
     *                                ponent)
     *
     *  (?:(?<patch>\d+)){1}))+ <-- This captures from 1 or more numbers till a period
     *                              again, again 1 time, It stops at a white space or
     *                              anything other than a number.
     *
     *                              * It's named the patch group (patch version group)
     *                              * It also closes off the version and required gro-
     *                                ups.
     *                              * The "+" following is the "+" of the required gr-
     *                                oups capture, enforcing that everything we've
     *                                covered so far must have happened 1 time, or no
     *                                match will be made at all (one will not be repo-
     *                                rted back, even if part of the components are
     *                                matched).
     *
     *
     *  (?:(?<prerelease>-[\+\!\~a-zA-Z0-9]+))?
     *
     *                          The above regex follows all which we've covered alrea-
     *                          dy, and states that there is a capture group, named
     *                          'prerelease', which consists of a hyphen, followed by
     *                          any character from a list which has been provided.
     *
     *                              * The hyphen must immediately follow the required
     *                                group in order for this match to succeed.
     *                              * The '[]' denotes a list, which includes \+, \!,
     *                                \~, a-z, A-Z, 0-9. Any other character fails -
     *                                or ends if following an allowed character - the
     *                                match.
     *                              * The '?' means that the entire match can possibl-
     *                                y exist, but does not have to.
     *                                if it does it will be matched, otherwise we wil-
     *                                l still be returned with the required match.
     *
     * And this is the regex we will use to parse for the semver, within a JSDoc-style
     * header, in any file that is passed to this plugin.
     *
     * The options passed to this plug-in will tell us whether or not we will be repla-
     * cing the entire semver string, or bumping a particular component.,
     *
     * We'll use this regexp in a string.replace function - so that we can actually pr-
     * operly replace the string if need be.
     *
     * @arg { RegExp }
     */
    regex: RegExp;
    /**
     * Holds the default preKey,key, and postKey components of our plug-in's regex.
     *
     * @arg { Object }
     */
    regexPieces: {
        preKey: string;
        postKey: string;
    };
    /**
     * Class constructor
     *
     * @since 0.1.0
     */
    constructor();
    /**
     * Method which operates on a file stream
     *
     * @param { File|Stream|Buffer } file - A file stream, buffer, or string for which we will operate upon the contents of
     * @param { Object } options - An object who's properties are options to this method
     *
     * @return { Stream } The file stream, with all operations applied
     *
     * @since 0.1.0
     */
    bumpVersion(file: any, options: any): any;
    /**
     * Method which modifies a JSDoc-based version tag and its accompanying value based upon values supplied to the calling parent.
     *
     * @param { string } fullMatch - The full match from the regex
     * @param { string } group1    - The first nested capture group, includes "@version:" through to the end of the third semver component
     * @param { string } group2    - The second nested capture group, includes "@version" through to the semver, but not including any semver components
     * @param { string } group3    - The third nested capture group, includes all semver components save prerelease (major, minor, patch)
     * @param { string } group4    - The first of 4 capture groups nested 3 levels in, captures the major semver component
     * @param { string } group5    - The second of 4 capture groups nested 3 levels in, captures the minor semver component
     * @param { string } group6    - The third of 4 capture groups nested 3 levels in, captures the patch semver component
     * @param { string } group7    - The fourth of 4 capture groups nested 3 levels in, captures the prerelease semver component (if exists)
     */
    replaceVersion(fullMatch: any, group1: any, group2: any, group3: any, group4: any, group5: any, group6: any, group7: any): any;
}
