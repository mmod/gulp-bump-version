/*-----------------------------------------------------------------------------
 * @package:    Kwaeri web developer tools - Build Automaton
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import uglifyes from 'uglify-es';                   // To minify es6+ JavaScript syntax
import composer from 'gulp-uglify/composer';        // Allows gulp-uglify to use an alternate uglify module (default is uglify-js)
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
import del from 'del';                              // Used by our clean process, for the most part -
import runsequence from 'run-sequence';             // For running tasks sequentially
import bump from './index';                         // To increment our own files (most of them, we unfortunately cannot increment
                                                    // our test files since they use the default tags as our plugin source files use
                                                    // and it messes mocha all up! At least for now...)


// GLOBALS
let minify      = composer( uglifyes, console );        // Replacing the uglify processor with one which supports es6+
let gtsProject  = gts.createProject( 'tsconfig.json' ); // A preliminary for transpiling typescript using microsoft's compiler

/*
COLOR REFERENCES

Reset = "\x1b[0m", Bright = "\x1b[1m", Dim = "\x1b[2m", Underscore = "\x1b[4m", Blink = "\x1b[5m", Reverse = "\x1b[7m"
Hidden = "\x1b[8m"

FgBlack = "\x1b[30m", FgRed = "\x1b[31m", FgGreen = "\x1b[32m". FgYellow = "\x1b[33m", FgBlue = "\x1b[34m", FgMagenta = "\x1b[35m"
FgCyan = "\x1b[36m", FgWhite = "\x1b[37m"

BgBlack = "\x1b[40m", BgRed = "\x1b[41m", BgGreen = "\x1b[42m", BgYellow = "\x1b[43m", BgBlue = "\x1b[44m", BgMagenta = "\x1b[45m"
BgCyan = "\x1b[46m", BgWhite = "\x1b[47m"
*/
let consoleAlert= '\x1b[46m\x1b[30m';                   // Colorization for alerts
let resetColors = '\x1b[0m';                            // Reset Colorization

const args =                                            // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check that we suppli
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}

// If type was provided, overwrite the options accordingly
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}


// Bump the version in our projects files (called manually, or through the build-release task)
gulp.task
(
    'bump-file-versions',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version strings in project files...', ' ' );

        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit out package.json: 'dist/src/**/*.js',
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}',
                '!test{,/**}',
                '!dist/test{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-project-version',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version string in package.json...', ' ' );

        // Target out package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'compile-typescript',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Compiling Typescript sources into JavaScript...'  );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'dist' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'generate-typescript-declarations',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Generating Typescript declarations...' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'dist' ) );
    }
);


// Watches for changes in TS & SCSS files
gulp.task
(
    'watch',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Watching for changes to SCSS source...' );

        gulp.watch( 'src/**/*.ts', ['compile-typescript'] );
    }
);


// Optimzes JS files
gulp.task
(
    'optimize-js',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying test JavaScript source files...' );

        //return gulp.src( [ 'dist/js/**/*.js', '!dist/js/kwaeri-ux/*.js' ] )
        return gulp.src( [ 'dist/**/*.js', '!dist/index.js' ] )
        .pipe( minify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);


// Copy index file(s) to the root of the project
gulp.task
(
    'move-index',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Moving index to the project root...' );

        //return gulp.src( [ 'dist/js/**/*.js', '!dist/js/kwaeri-ux/*.js' ] )
        return gulp.src( [ 'dist/index.js', 'dist/index.d.ts' ] )
        .pipe( minify() )
        .pipe( gulp.dest( '.' ) );

    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:dist-index',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning dist/index.*...' );

        let deleteList = ( !args.alt ) ? ['dist/index.d.ts', 'dist/index.js'] : ['dist/index.d.ts', 'dist/index.js'];
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:dist',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning build result...' );

        let deleteList = ( !args.alt ) ? 'dist' : 'dist';
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:index',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning dist/index.*...' );

        let deleteList = ( !args.alt ) ? ['index.d.ts', 'index.js'] : ['index.d.ts', 'index.js'];
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean',
    callback =>
    {
        runsequence
        (
            'clean:dist',
            'clean:index',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'bump-version',
    callback =>
    {
        console.log( ( ( args.alt ) ? consoleAlert + '[Build-alt]: ' : consoleAlert + '[Build]: ' )  + consoleAlert + '!!!MAKE SURE THAT YOU MANUALLY BUMP THE TEST FILE(s) FILE VERSIONS IN THE HEADER COMMENTS!!!' + resetColors );
        console.log( ( ( args.alt ) ? consoleAlert + '[Build-alt]: ' : consoleAlert + '[Build]: ' )  + consoleAlert + ' - `test/test.ts`' + resetColors );
        console.log( ( ( args.alt ) ? consoleAlert + '[Build-alt]: ' : consoleAlert + '[Build]: ' )  + consoleAlert + ' - `dist/test/test.js`' + resetColors );

        runsequence
        (
            'bump-project-version',
            'bump-file-versions',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'default',
    callback =>
    {
        runsequence
        (
            'clean',
            'compile-typescript',
            'generate-typescript-declarations',
            'optimize-js',
            'move-index',
            'clean:dist-index',
            callback
        );
    }
);

