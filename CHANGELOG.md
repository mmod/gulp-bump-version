# Changelog

## Unreleased

## 0.1.2

### Added

### Changed

### Removed

## 0.1.1

### Added

* Self-dependency for automated file-header version increment in gulpfile.babel.js
* CHANGELOG.md added for managing the changelog
  * Initial release added to the changelog

### Changed

* Bits of the `README.md` which are static, but which are fully covered in the [documentation project wiki](https://gitlab.com/mmod/documentation/wikis/home)
  * Removed full text and replaced with references to the respective wiki pages

### Removed

* Chalk dev-dependency, managing colors ourselves now (see gulpfile.babel.js).

## 0.1.0 - 2018-06-04 11:53:00 EST

### Initial Release
