# gulp-bump-version

[![pipeline status](https://gitlab.com/mmod/gulp-bump-version/badges/master/pipeline.svg)](https://gitlab.com/mmod/gulp-bump-version/commits/master)  [![coverage report](https://gitlab.com/mmod/gulp-bump-version/badges/master/coverage.svg)](https://mmod.gitlab.io/gulp-bump-version/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1888/badge)](https://bestpractices.coreinfrastructure.org/projects/1888)

gulp-bump-version eases and automates the management of semver version strings in the source code of almost any file type.

If you like our software, please consider making a donation. Donations help greatly to maintain the Massively Modified network and continued support of our open source offerings:

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

## Table of Contents

* [The Implementation (How it works)](#the-implementation)
* [Using gulp-bump-version](#using-gulp-bump-version)
  * [Basic Installation](#basic-installation)
  * [Integration](#integration)
  * [Basic Usage & Examples](#basic-usage-examples)
  * [Customization](#customization)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

gulp-bump-version searches files piped from a gulp task for a `@version` tag (or key), and replaces the *semver* value associated with the tag based on the options you supply.  Typically you'd find these version tags within a file header comment, such as a JSDoc-based header comment - though the plug-in is not restricted by file type, nor does it require the tag to be within a comment block at all.

At Massively Modified, our project files always contain a header comment (when applicable) which look like the following snippet:

```javascript
/*-----------------------------------------------------------------------------
 * @package;    gulp-bump-version
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/
```

And our methods within the file tend to have comments that reference an `@since` tag, which defines since which version the API for that method has been defined:

```javascript
/**
 * A dummy method for demonstration purposes
 *
 * @param { string } argA  - An argument named A
 * @param { object } optA  - An option container named A
 *
 * @return void
 *
 * @since 0.1.0
 */
 function dummy( argA, optA )
 {
     ...
 }
```

We got tired of manually updating the tag in the header comments (or more accurately, tired of *forgetting* to!), and decided to write this Gulp plug-in to handle it for us. While developing the Gulp plug-in, we thought it would be nice to modify it a bit so that virtually anyone could make use of it, so we designed it with a few goodies to enable said flexibility!

Read further to find out how to use the plug-in as is, and how to customize it for your own needs.

## Using gulp-bump-version

To use gulp-bump-version in your own project/application, you must install and include it in your project:

### Basic Installation

You can obtain gulp-bump-version via npm:

```bash
npm install gulp-bump-version
```

### Integration

Import the plug-in in your application's `gulpfile` ( or `gulpfile.babel.js`):

ES6

```javascript
import bump from 'gulp-bump-version'
```

ES5:

```javascript
var bump = require( 'gulp-bump-version' );
```

#### Basic Usage & Examples

Let's start with how to use it with out any modification to the regex, assuming you also use the same style for writing out your file versions:

```javascript
// Bump patch version
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Incrementing the patch version by 1' );

        return gulp.src( ['js/**/*.js'] )
        .pipe( bump( { type: 'patch' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

```javascript
// Bump minor version
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Incrementing the minor version by 1' );

        return gulp.src( ['js/**/*.js'] )
        .pipe( bump( { type: 'minor' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

```javascript
// Bump major version
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Incrementing the major version by 1' );

        return gulp.src( ['js/**/*.js'] )
        .pipe( bump( { type: 'major' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

```javascript
// Bump prerelease version
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Incrementing the prerelease version by 1' );

        return gulp.src( ['js/**/*.js'] )
        .pipe( bump( { type: 'prerelease' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

```javascript
// Set version
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Manually setting the version' );

        return gulp.src( ['js/**/*.js'] )
        .pipe( bump( { version: '7.2.0' } ) )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

##### Customization

The regular expression used assumes a few things:

* That there are no breaks between the `@version:` key portion and the semver components portion. The only thing allowed between the two poritons are space characters: `[ ]*`.
  * Anything can be on the line in-front of the `@version:` portion. If anything other than a number is found after, even a new-line character, the match fails.
* That the prerelease component will exist if the major, minor, and patch components are immediately followed by a hyphen `-` and one or more of the following allowable characters: `[\~\!\+a-zA-Z0-9]`.
  * Following the prerelease component, any invalid (non-allowed) character stops the match.
* That all three 'normal' components exist, if only major and minor exist, for example, the match breaks. The prerelease does not *have* to be there, but the major, minor, and patch components **must** be defined.

Other than that, the version tag and value can be in any file type, and anything else can be on the same line. Now let's go over how to modify the regex a bit so that you might apply it to a special circumstance.

```javascript
// Customizing the regex
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Manually setting the version' );

        return gulp.src( ['js/**/*.js'] )
        .pipe
        (
            bump
            (
                {
                    type: 'patch',
                    key: '@version' // Removed the semi-colon, make sure you escape escaped characters!!!
                }
            )
        )
        .pipe( gulp.dest( 'dist' ) );
    }
);
```

By utilizing the `key` option parameter you can change the key. With this method of customization, all the same rules apply - you simply change the key value which is to be found preceeding the semver component string.

The following snippet shows how you might modify the version of a package.json file for a node project:

```javascript
// Updating the version in ./package.json
gulp.task
(
    'bump-version',
    () =>
    {
        console.log( 'Incrementing the patch revision' );

        return gulp.src( ['package.json'] )
        .pipe
        (
            bump
            (
                {
                    type: 'patch',
                    key: '"version": "' // This will match the version in our package.json file!!
                }
            )
        )
        .pipe( gulp.dest( '' ) );
    }
);
```

Here we've completely changed the key, so that we can match the version string in our `package.json` file. You could even remove the key altogether, allowing to match just the semver components - you simply do this at your own risk!

For an example showing how to accept arguments in your gulpfile, and build a bump-version task which reads those arguments to support all the variants under a single task - dynamiclly; take a look at the [Gulpfile.babel.js](https://gitlab.com/mmod/kwaeri-user-experience/blob/master/gulpfile.babel.js) file in our `@kwaeri/user-experience` project..

We thought about providing a means to customize the full regex, yet that would go beyond the scope of what this plug-in is intended to do (there are other plugins out there, such as gulp-replace, which have a main focus of allowing you to search and replace just about anything). Besides, the 7 capture interface this plug-in uses would feed some strict requirements in regex design, and anyone digging that far into it could customize the source for their own purposes to begin with.

That about sums it up for how to use this plug-in. Feel free to read on further to learn how to contribute!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/gulp-bump-version/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/gulp-bump-versione@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/gulp-bump-version/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
