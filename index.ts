/*-----------------------------------------------------------------------------
 * @package;    gulp-bump-version
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/
'use strict';

import { Transform } from 'stream';
import bump from './dist/src/bump';

module.exports = function( options )
{
    // monkey-patching transform
    let transformStream = new Transform( { objectMode: true } );

    /**
     * Monkey-patached _transform method
     *
     * @param { Buffer|string } file
     * @param { string= } encoding - ignored if file contains a Buffer
     * @param { function( Error, object ) } callback - Call this function (optionally with an error argument and data) when you are done processing the supplied chunk.
     */
    transformStream._transform = function( file, encoding, callback )
    {
        let error = null,
            bumper = new bump(),
            output = ( bumper as any ).bumpVersion( file, options );

        if( output == -3 )
        {
            error = "ERROR: You must provide an options argument as an object with a property of version or type, appropriately set for modifying file versions as you prefer. Visit http://gitlab.com/mmod/gulp-bump-version#basic-usage-examples for more information.";
        }

        callback( error, output );
    };

    return transformStream;
};

